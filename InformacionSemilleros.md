**SEMILLERO SIEDSS**

**Misión**

Forjar y adecuar proyectos de investigación científica para generar
conocimiento útil de los diferentes materiales, procesos y técnicas en
el desarrollo de obras civiles, generando sentido investigativo entre
los estudiantes del programa, con el soporte de un grupo de
profesionales comprometidos con el avance de las técnicas, materiales y
la calidad de los mismos en la ejecución de los procesos constructivos.

**Visión**

Convertirse en un grupo líder de propuestas de investigación de alta
calidad para la formación de excelentes investigadores que puedan
tributar conocimiento de valor, demostrando la mejor calidad de
formación académica e investigativa con sentido de responsabilidad ética
y socia

**Líneas de investigación**

Materiales de Ingeniería, Diseño y Construcción de Vías.

**GESTIÓN DE RECURSOS HÍDRICOS Y SANEAMIENTO BÁSICO**

**HYDROS**

**Misión**

El semillero de investigación en Gestión de Recursos Hídricos y
Saneamiento Básico -- HYDROS fomenta la construcción del espíritu
científico de los estudiantes mediante la formación crítica y reflexiva
de estos para abordar problemas de la ciencia y la sociedad relacionados
con la gestión integral de los recursos hídricos superficiales y
subterráneos, los procesos de tratamiento de agua potable y residuales,
la captación, transporte y adaptación de modelos para la gestión de
redes de distribución y el desarrollo sostenible.

**Visión**

Hydros será en el año 2025 un semillero de investigación consolidado que
formula propuestas de investigación y desarrolla proyectos de impacto
regional, nacional e internacional que aporta soluciones innovadoras a
problemáticas de la ciencia y la sociedad relacionados con la gestión
integral de los recursos hídricos bajo el enfoque

**Líneas de investigación**

Energías, aguas y medio ambiente

**Proyectos en curso**

-   Sistema de monitoreo en tiempo real del nivel freático y de calidad
    del acuífero de la ciudad de Santa Marta.

-   Determinación de las concentraciones de elementos traza metálicos a
    partir de registros sedimentarios de lagos de alta montaña de La
    Sierra Nevada De Santa Marta.

-   Degradación de Micro contaminantes orgánicos presentes en aguas
    residuales mediante procesos de Oxidación Avanzada por
    Fotocatálisis.

-   Concreción de un vivero de especies de manglar bajo el enfoque de
    laboratorio vivo para la reforestación de zonas degradadas en la
    Ciénaga Grande De Santa Marta Municipio De Pueblo viejo- Colombia.

**SEMILLERO DE APROVECHAMIENTO SOSTENIBLE DE LOS RECURSOS HÍDRICOS E
HIDRÁULICOS**

**ASRHID**

**Misión**

El semillero del aprovechamiento sostenible de los recursos hídricos e
hidráulicos (ASRHID) de la Universidad Cooperativa de Colombia sede
Santa Marta busca crear espacios de profundización en la aplicación de
la ingeniería hidráulica a través del trabajo investigativo. Lo
anterior, se logra a través del desarrollo de proyectos investigativos
que propendan a dar solución a las problemáticas relacionadas al recurso
hídrico e hidráulico en la región y en el país, a la vez que se
despiertan en los estudiantes las capacidades y aptitudes propias del
trabajo investigativo a través de espacios de reflexión, debate y
análisis.

**Visión**

El semillero de investigación se proyecta al año 2028 como un semillero
de ingeniería hidráulica que será reconocido en la región Caribe de
Colombia, y en el país, por sus aportes al desarrollo de la calidad de
vida y la economía de la región a través de la investigación y del uso
de nuevas tecnologías.

**Línea de investigación**

Ingeniería hidráulica

**SEMILLERO I+D+C**

**Misión**

El Semillero de Investigación \"I+D+C\" tiene como misión fomentar la
investigación y el desarrollo tecnológico en el campo de la ingeniería
electrónica, mediante la formación de un equipo interdisciplinario de
estudiantes y docentes, que promueva la innovación y la aplicación
práctica del conocimiento adquirido en el aula de clase brindando
soluciones a problemáticas regionales que aporten a la conservación de
los recursos naturales y utilización de recursos energéticos renovables
que minimicen el impacto sobre el ecosistema .

**Visión**

El Semillero de Investigación \"I+D+C\" tiene como visión ser reconocido
como un referente académico en el campo de la ingeniería electrónica, a
nivel regional y nacional, por su compromiso con la excelencia académica
y la generación de conocimiento innovador.

**Línea de investigación**

Ingeniería Electrónica, enfocada en el diseño, desarrollo e
implementación de soluciones tecnológicas innovadoras.

**SEMILLERO MODELACION ESTRUCUTRAL, SIMULACIÓN Y ANALISIS MESA**

**Misión**

El semillero de investigación en ingeniería estructural -- MESA
"MODELACION ESTRUCTURAL SIMULACIÓN Y ANÁLISIS", fomenta la construcción
del espíritu científico de los estudiantes mediante la formación crítica
y reflexiva de estos, para abordar problemas de la ciencia y la sociedad
relacionados con la ingeniería estructural.

**Visión**

MESA será en el año 2023 un semillero de investigación consolidado que
formula propuestas de investigación y desarrolla proyectos de impacto
regional, nacional e internacional que aporta soluciones innovadoras a
problemáticas de la ciencia y la sociedad relacionados con la Ingeniería
estructural.

**Línea de investigación**

Estructuras, materiales y suelos

**SEMILLERO MOVITRANS**

**Misión**

El semillero movilidad y transporte se dedicará a investigar sobre los
temas relacionados con la movilidad y el transporte en entornos urbanos
y rurales, con el fin de proponer soluciones a problemáticas desde los
principios de la ingeniería de tránsito.

**Visión**

El semillero de movilidad y transporte será en 2025 un semillero
referente en investigaciones de movilidad y transporte, haciéndose
visible en la participación de eventos de semilleros y participación en
proyectos de investigación del área de transporte.

**Línea de investigación**

Vías y carreteras.

**SEMILLERO SISTEMA DE INNOVACION E INGENIO COMPUTACIONAL**

**SI^2^C**

**Misión**

El semillero sistema de innovación e ingenio computacional SI^2^C de la
Universidad Cooperativa de Colombia sede Santa Marta, tiene como misión
fomentar la investigación como apoyo y solución a las diferentes
problemáticas del entorno. Así como propiciar espacios de reflexión,
debate y análisis sobre diferentes temas relacionados con las
tecnologías de la información y las comunicaciones. Buscando despertar
capacidades y aptitudes propias del trabajo de investigación.

**Visión**

El semillero de investigación se proyecta dentro de su ciclo de
formación las siguientes metas:

-   Contribuir a desarrollar y fortalecer juntamente con los
    estudiantes, docentes y egresados; la investigación al interior de
    la universidad cooperativa de Colombia sede santa marta.

-   Formar parte de una red de investigación compuesta por
    universidades, empresas, y asociaciones, para la formulación y
    ejecución de proyectos que contribuyan al desarrollo social de la
    región y el país.

**Línea de investigación**

Tecnologías de la Información y de las Comunicaciones
